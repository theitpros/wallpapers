

**Where Did I Get These?**

I grabed most of these from Derek Taylor's (DT's)github [https://gitlab.com/dwt1/wallpapers] and added some of my favorites as well.

**Style of Wallpapers**

The vast majority of these wallpapers are nature and landscape photos.  There are a few abstract art wallpapers mixed in.

**Ownership**

Because I downloaded most of these from sites like Imgur and /wg/, I have no way of knowing if there is a copyright on these images. If you find an image hosted in this repository that is yours and of limited use, please let me know and I will remove it.
